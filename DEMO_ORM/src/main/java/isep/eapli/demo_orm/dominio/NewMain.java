/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import isep.eapli.demo_orm.apresentacao.GrupoAutomovelUI;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author João Pedro Lourenço Domingues <1150352@isep.ipp.pt>
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GrupoAutomovelUI gaUI = new GrupoAutomovelUI();
        
        gaUI.registarGA();

//        GrupoAutomovel ga = new GrupoAutomovel("test", 4, "classe");
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("DEMO_ORMPU");
//        EntityManager em = emf.createEntityManager();
//        em.getTransaction().begin();
//        em.persist(ga);
//        em.getTransaction().commit();
//        em.close();
//        emf.close();
//        System.out.println("ID gerado: " + ga.getId());
    }
    
}
